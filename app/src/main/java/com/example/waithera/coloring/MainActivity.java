package com.example.waithera.coloring;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.VideoView;

import java.util.LinkedList;
import java.util.Queue;

import android.media.MediaPlayer;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.VideoView;

public class MainActivity extends AppCompatActivity {

    // Create a VideoView variable, a MediaPlayer variable, and an int to hold the current
    // video position.
//    private VideoView videoBG;
//    MediaPlayer mMediaPlayer;
//    int mCurrentVideoPosition;
//    private Button next;
    private static int SPLASH_TIME_OUT=3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                Intent lessons=new Intent(MainActivity.this,Alphabets.class);
                startActivity(lessons);
                finish();
            }
        },SPLASH_TIME_OUT);
        // Hook up the VideoView to our UI.
//        videoBG = (VideoView) findViewById(R.id.videoView);
//
//        // Build your video Uri
//        Uri uri = Uri.parse("android.resource://" // First start with this,
//                + getPackageName() // then retrieve your package name,
//                + "/" // add a slash,
//                + R.raw.background_three); // and then finally add your video resource. Make sure it is stored
//        // in the raw folder.
//
//        // Set the new Uri to our VideoView
//        videoBG.setVideoURI(uri);
//        // Start the VideoView
//        videoBG.start();
//
//        // Set an OnPreparedListener for our VideoView. For more information about VideoViews,
//        // check out the Android Docs: https://developer.android.com/reference/android/widget/VideoView.html
//        videoBG.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
//            @Override
//            public void onPrepared(MediaPlayer mediaPlayer) {
//                mMediaPlayer = mediaPlayer;
//                // We want our video to play over and over so we set looping to true.
//                mMediaPlayer.setLooping(true);
//                // We then seek to the current posistion if it has been set and play the video.
//                if (mCurrentVideoPosition != 0) {
//                    mMediaPlayer.seekTo(mCurrentVideoPosition);
//                    mMediaPlayer.start();
//
//                }
//            }
//        });
//
//        next=(Button)findViewById(R.id.next);
//        next.setOnClickListener(new View.OnClickListener(){
//
//            @Override
//            public void onClick(View v) {
//                Intent alphabets=new Intent(MainActivity.this,Alphabets.class);
//                startActivity(alphabets);
//            }
//        });
    }

    /*================================ Important Section! ================================
    We must override onPause(), onResume(), and onDestroy() to properly handle our
    VideoView.
     */

//    @Override
//    protected void onPause() {
//        super.onPause();
//        // Capture the current video position and pause the video.
//        mCurrentVideoPosition = mMediaPlayer.getCurrentPosition();
//        videoBG.pause();
//    }
//
//    @Override
//    protected void onResume() {
//        super.onResume();
//        // Restart the video when resuming the Activity
//        videoBG.start();
//    }
//
//    @Override
//    protected void onDestroy() {
//        super.onDestroy();
//        // When the Activity is destroyed, release our MediaPlayer and set it to null.
//        mMediaPlayer.release();
//        mMediaPlayer = null;
//
//
//    }

}