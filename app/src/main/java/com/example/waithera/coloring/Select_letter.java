package com.example.waithera.coloring;

import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

import butterknife.BindView;
import butterknife.ButterKnife;

import  com.example.waithera.coloring.pager.Pager;

public class Select_letter extends AppCompatActivity {
private PagerAdapter mPagerAdapter;
@BindView(R.id.viewPager)ViewPager viewPager;
MenuItem menuItem;
Menu menu;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_letter);

        ButterKnife.bind(this);
        Pager adapter = new Pager(getSupportFragmentManager());
        viewPager.setAdapter(adapter);
        viewPager.setCurrentItem(0);
    }
}

