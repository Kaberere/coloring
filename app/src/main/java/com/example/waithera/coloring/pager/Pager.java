package com.example.waithera.coloring.pager;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.example.waithera.coloring.fragments.Fragment_a;
import com.example.waithera.coloring.fragments.Fragment_b;
import com.example.waithera.coloring.fragments.Fragment_c;
import com.example.waithera.coloring.fragments.Fragment_d;
import com.example.waithera.coloring.fragments.Fragment_e;
import com.example.waithera.coloring.fragments.Fragment_f;
import com.example.waithera.coloring.fragments.Fragment_g;
import com.example.waithera.coloring.fragments.Fragment_h;
import com.example.waithera.coloring.fragments.Fragment_i;
import com.example.waithera.coloring.fragments.Fragment_j;
import com.example.waithera.coloring.fragments.Fragment_k;
import com.example.waithera.coloring.fragments.Fragment_l;
import com.example.waithera.coloring.fragments.Fragment_m;
import com.example.waithera.coloring.fragments.Fragment_n;
import com.example.waithera.coloring.fragments.Fragment_o;
import com.example.waithera.coloring.fragments.Fragment_p;
import com.example.waithera.coloring.fragments.Fragment_q;
import com.example.waithera.coloring.fragments.Fragment_r;
import com.example.waithera.coloring.fragments.Fragment_s;
import com.example.waithera.coloring.fragments.Fragment_t;
import com.example.waithera.coloring.fragments.Fragment_u;
import com.example.waithera.coloring.fragments.Fragment_v;
import com.example.waithera.coloring.fragments.Fragment_w;
import com.example.waithera.coloring.fragments.Fragment_x;
import com.example.waithera.coloring.fragments.Fragment_y;
import com.example.waithera.coloring.fragments.Fragment_z;


public class Pager extends FragmentStatePagerAdapter {

    private String[] titles = {"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","q","r","s","t","u","v","w","x","y","z"};

    public Pager(FragmentManager fm){
        super(fm);

    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }

    @Override
    public Fragment getItem(int i) {
        switch (i){
            case 0:
                return new Fragment_a();
            case 1:
                return new Fragment_b();
            case 3:
                return new Fragment_c();
            case 4:
                return new Fragment_d();
            case 5:
                return new Fragment_e();
            case 6:
                return new Fragment_f();
            case 7:
                return new Fragment_g();
            case 8:
                return new Fragment_h();
            case 9:
                return new Fragment_i();
            case 10:
                return new Fragment_j();
            case 11:
                return new Fragment_k();
            case 12:
                return new Fragment_l();
            case 13:
                return new Fragment_m();
            case 14:
                return new Fragment_n();

            case 15:
                return new Fragment_o();

            case 16:
                return new Fragment_p();
            case 17:
                return new Fragment_q();
            case 18:
                return new Fragment_r();
            case 19:
                return new Fragment_s();
            case 20:
                return new Fragment_t();
            case 21:
                return new Fragment_u();
            case 22:
                return new Fragment_v();
            case 23:
                return new Fragment_w();
            case 24:
                return new Fragment_x();
            case 25:
                return new Fragment_y();
            case 26:
                return new Fragment_z();






















            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return titles.length;
    }
}
