package com.example.waithera.coloring.fragments;

import android.media.MediaPlayer;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.VideoView;

import com.example.waithera.coloring.R;

import butterknife.ButterKnife;

/**
 * Created by Waithera on 16/11/2018.
 */

public class Fragment_c extends Fragment {
    private VideoView view;
    MediaPlayer mMediaPlayer;
    int mCurrentVideoPosition;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_c_activity,container,false);
        ButterKnife.bind(this,v);

        view = (VideoView)v.findViewById(R.id.videoView);

        String path = "android.resource://" + getActivity().getPackageName() + "/" + R.raw.background;
        view.setVideoURI(Uri.parse(path));
        view.start();
        view.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(MediaPlayer mediaPlayer) {
                mMediaPlayer = mediaPlayer;
                // We want our video to play over and over so we set looping to true.
                mMediaPlayer.setLooping(true);
                // We then seek to the current posistion if it has been set and play the video.
                if (mCurrentVideoPosition != 0) {
                    mMediaPlayer.seekTo(mCurrentVideoPosition);
                    mMediaPlayer.start();
                    onPause();
                }
            }
        });
        return v;

    }
//    @Override
//    public void onPause() {
//        super.onPause();
//        // Capture the current video position and pause the video.
//        mCurrentVideoPosition = mMediaPlayer.getCurrentPosition();
//        view.pause();
//    }

    @Override
    public  void onResume() {
        super.onResume();
        // Restart the video when resuming the Activity
        view.start();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        // When the Activity is destroyed, release our MediaPlayer and set it to null.
        mMediaPlayer.release();
        mMediaPlayer = null;


    }


}
