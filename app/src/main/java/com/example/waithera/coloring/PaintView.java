package com.example.waithera.coloring;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BlurMaskFilter;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.EmbossMaskFilter;
import android.graphics.MaskFilter;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.os.AsyncTask;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.MotionEvent;
import android.view.View;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by Waithera on 06/11/2018.
 */

public class PaintView extends View {
    public static int BRUSH_SIZE = 100;
    public static final int DEFAULT_COLOR = Color.BLACK;
    public static final int DEFAULT_BG_COLOR = Color.BLUE;
    private static final float TOUCH_TOLERNCE = 4;
    private float mx, my;
    private Path mPath;
    private Paint mPaint;
    private ArrayList<FingerPath> paths = new ArrayList<>();
    private int currentColor;
    private int backgroundColor = DEFAULT_BG_COLOR;
    private int strokeWidth;
    private boolean emboss;
    private boolean blur;
    private MaskFilter mEmboss;
    private MaskFilter mBlur;
    private Bitmap mBitmap, mBitmap_two;
    private Canvas mCanvas;
    private Bitmap bgImg;
    private Paint mBitmapPaint = new Paint(Paint.DITHER_FLAG);
    //added
    final Point p1 = new Point();

    public PaintView(Context context) {
        super(context);
    }

    public PaintView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        mPaint = new Paint();
        mPaint.setAntiAlias(true);
        mPaint.setDither(true);
        mPaint.setColor(DEFAULT_COLOR);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setXfermode(null);
        mPaint.setAlpha(0xff);

        mEmboss = new EmbossMaskFilter(new float[]{1, 1, 1}, 0.4f, 6, 3.5f);
        mBlur = new BlurMaskFilter(5, BlurMaskFilter.Blur.NORMAL);

        //added
//        mBitmap_two = BitmapFactory.decodeResource(getResources(),
//                R.drawable.a).copy(Bitmap.Config.ARGB_8888, true);

    }

    //    @Override
//    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
//        super.onSizeChanged(w, h, oldw, oldh);
//        Resources resources = getResources();
//        mBitmap = BitmapFactory.decodeResource(resources, R.drawable.a);
//        mBitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
//        mCanvas = new Canvas(mBitmap);
//
//    }
    public void init(DisplayMetrics metrics) {
        int height = metrics.heightPixels;
        int width = metrics.widthPixels;

         mBitmap = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
        //added
   mBitmap_two = BitmapFactory.decodeResource(getResources(),
              R.drawable.a).copy(Bitmap.Config.ARGB_8888, true);

        mCanvas = new Canvas(mBitmap);

        currentColor = DEFAULT_COLOR;
        strokeWidth = BRUSH_SIZE;
    }

    public void normal() {
        emboss = false;
        blur = false;

    }

    public void emboss() {
        emboss = true;
        blur = false;
    }

    public void blur() {
        emboss = false;
        blur = true;
    }

    public void clear() {
        backgroundColor = DEFAULT_BG_COLOR;
        paths.clear();
        normal();
        invalidate();
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.save();
        // mCanvas.drawColor(backgroundColor);
//added
        // mCanvas.drawBitmap(mBitmap_two, 0, 0, null);
       // canvas.drawText("A", 300, 150, mPaint);
        for (FingerPath fp : paths) {
            mPaint.setColor(fp.color);
            mPaint.setStrokeWidth(fp.strokeWidth);
            mPaint.setMaskFilter(null);

            if (fp.emboss)
                mPaint.setMaskFilter(mEmboss);
            else if (fp.blur)
                mPaint.setMaskFilter(mBlur);

            mCanvas.drawPath(fp.path, mPaint);

        }
        mBitmapPaint.setColor(backgroundColor);
        canvas.drawBitmap(mBitmap_two, 0, 0, mBitmapPaint);
        canvas.restore();
    }

    private void touchStart(float x, float y) {
        mPath = new Path();

        FingerPath fp = new FingerPath(currentColor, emboss, blur, strokeWidth, mPath);
        paths.add(fp);

        mPath.reset();
        mPath.moveTo(x, y);
        mx = x;
        my = y;

    }

    private void touchMove(float x, float y) {
        float dx = Math.abs(x - mx);
        float dy = Math.abs(y - my);

        if (dx >= TOUCH_TOLERNCE || dy >= TOUCH_TOLERNCE) {
            mPath.quadTo(mx, my, (x + mx) / 2, (y + my) / 2);
            mx = x;
            my = y;
        }

    }

    private void touchUp() {

        mPath.lineTo(mx, my);
        mPaint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.SCREEN));
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {

        float x = event.getX();
        float y = event.getY();

        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
             //   touchStart(x, y);
//int sourceColor=mBitmap.getPixel((int) x, (int) y);
//                int desColor = mPaint.getColor();
                // pass the bitmap you want paintt

                // FloodFill(  mBitmap, new Point((int) x, (int) y) , sourceColor , desColor  );

                p1.x = (int) x;
                p1.y = (int) y;
                final int sourceColor = mBitmap_two.getPixel((int) x, (int) y);
                final int targetColor = mPaint.getColor();
                new TheTask(mBitmap_two, p1, sourceColor, targetColor).execute();

                invalidate();
                break;
//            case MotionEvent.ACTION_MOVE:
//                touchMove(x, y);
//                invalidate();
//                break;
//            case MotionEvent.ACTION_UP:
//                touchUp();
//                invalidate();
//                break;

        }
        return true;
    }

//    private void FloodFill(Bitmap bmp, Point pt, int targetColor, int replacementColor) {
//        Queue<Point> q = new LinkedList<Point>();
//        q.add(pt);
//
//        while (q.size() > 0) {
//            Point n = q.poll();
//            if (bmp.getPixel(n.x, n.y) != targetColor)
//                continue;
//
//            Point w = n, e = new Point(n.x + 1, n.y);
//            while ((w.x > 0) && (bmp.getPixel(w.x, w.y) == targetColor)) {
//                bmp.setPixel(w.x, w.y, replacementColor);
//                if ((w.y > 0) && (bmp.getPixel(w.x, w.y - 1) == targetColor))
//                    q.add(new Point(w.x, w.y - 1));
//                if ((w.y < bmp.getHeight() - 1)
//                        && (bmp.getPixel(w.x, w.y + 1) == targetColor))
//                    q.add(new Point(w.x, w.y + 1));
//                w.x--;
//            }
//            while ((e.x < bmp.getWidth() - 1)
//                    && (bmp.getPixel(e.x, e.y) == targetColor)) {
//                bmp.setPixel(e.x, e.y, replacementColor);
//
//                if ((e.y > 0) && (bmp.getPixel(e.x, e.y - 1) == targetColor))
//                    q.add(new Point(e.x, e.y - 1));
//                if ((e.y < bmp.getHeight() - 1)
//                        && (bmp.getPixel(e.x, e.y + 1) == targetColor))
//                    q.add(new Point(e.x, e.y + 1));
//                e.x++;
//            }
//        }
//    }

    class TheTask extends AsyncTask<Void, Integer, Void> {

        Bitmap bmp;
        Point pt;
        int replacementColor, targetColor;

        public TheTask(Bitmap bm, Point p, int sc, int tc) {
            this.bmp = bm;
            this.pt = p;
            this.replacementColor = tc;
            this.targetColor = sc;
//            pd.setMessage("Filling....");
//            pd.show();
        }

        @Override
        protected void onPreExecute() {
            // pd.show();

        }

        @Override
        protected void onProgressUpdate(Integer... values) {

        }

        @Override
        protected Void doInBackground(Void... params) {
            FloodFill f = new FloodFill();
            f.floodFill(bmp, pt, targetColor, replacementColor);
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            // pd.dismiss();
            invalidate();
        }
    }

    public class FloodFill {
        public void floodFill(Bitmap image, Point node, int targetColor,
                              int replacementColor) {
            int width = image.getWidth();
            int height = image.getHeight();
            int target = targetColor;
            int replacement = replacementColor;
if(targetColor==0)return;
                if (target != replacement) {
                    Queue<Point> queue = new LinkedList<Point>();
                    queue.add(node);
                    do {


                        int x = node.x;
                        int y = node.y;
                        while (x > 0 && image.getPixel(x - 1, y) == target) {
                            x--;

                        }
                        boolean spanUp = false;
                        boolean spanDown = false;
                        while (x < width && image.getPixel(x, y) == target) {
                            image.setPixel(x, y, replacement);

                            if (!spanUp && y > 0
                                    && image.getPixel(x, y - 1) == target) {
                                queue.add(new Point(x, y - 1));

                                spanUp = true;
                            } else if (spanUp && y > 0
                                    && image.getPixel(x, y - 1) != target) {
                                spanUp = false;
                            }
                            if (!spanDown && y < height - 1
                                    && image.getPixel(x, y + 1) == target) {
                                queue.add(new Point(x, y + 1));
                                spanDown = true;
                            } else if (spanDown && y < height - 1
                                    && image.getPixel(x, y + 1) != target) {
                                spanDown = false;
                            }
                            x++;

                        }
                    } while ((node = queue.poll()) != null);
                }
            }
        }

    }
